<?php

use Arhitector\Yandex\Disk;
require_once __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../App/migration/dirHandler.php';
require __DIR__.'/../App/migration/fileHandler.php';
require __DIR__.'/../App/migration/helpers.php';
require __DIR__.'/../App/migration/uploadFileToYad.php';
require __DIR__.'/../App/migration/storeToArticle.php';
require __DIR__.'/../App/migration/logging.php';



// подключаем модуль импорта ENV
$dotenv = Dotenv\Dotenv::createImmutable('../config');
$dotenv->load();

// инициализация Яндекс Диска
$apikey=$_ENV['YD_API_KEY'];
$disk = new Disk($apikey);
$freeSpace=round($disk['free_space']/1024/1024);
$rootDir = $disk->getResource($_ENV['SOURCE_DIR']);
echo nl2br('Яндекс диск подключен. Ресурс '.$rootDir->get('name').'тип Папка: '.$rootDir->isDir().' Свободное место: '.$freeSpace.' Мб').PHP_EOL;
$rootDirList=$rootDir->items; // все объекты корневой папки

// Инициализация БД, используем библиотеку MySQLi (MySQL Improved)
//$KBLink = mysqli_connect($_ENV['BD_URL'], $_ENV['BD_USER'], $_ENV['BD_PASS']);
$KBLink = new mysqli($_ENV['BD_URL'], $_ENV['BD_USER'], $_ENV['BD_PASS'], $_ENV['BD_USER']);
if ($KBLink->connect_error) {
    die('Connect Error (' . $KBLink->connect_errno . ') ' . $KBLink->connect_error);
}
mysqli_set_charset($KBLink, "utf8");
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
print("Соединение установлено успешно");


foreach ($rootDirList as $item){
    $resultObject=[]; // результирующий объект файловой структуры статьи
    $idArticle=(int)$item->get('name'); // имя папки это ID статьи
    echo 'Index article '.$idArticle.PHP_EOL;
    $objectPath=$item->get('path'); // считываем файлы внутри папки
    $objResource=$disk->getResource($objectPath);
    $objList=$objResource->items;
    foreach ($objList as $item2){ // начинаем анализ содержимого папки
        $itemName=$item2->get('name');
        if ($item2->isDir()){
            $result=dirHandler($item2, $disk, $KBLink, $idArticle);
        }
        if ($item2->isFile()){
            $result=fileHandler($item2, $disk, $KBLink, $idArticle);
        }
        $resultObject[$itemName]=$result;
}
    storeToArticle($idArticle,$resultObject, $KBLink );
 }





