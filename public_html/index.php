<?php
require_once __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../templates/header.php';
use App\FileToServer;
use App\FromServerToYa;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// подключаем модуль импорта ENV
$dotenv = Dotenv\Dotenv::createImmutable($_SERVER['DOCUMENT_ROOT'].'/config');
$dotenv->load();
// инициализация подключения Яндекс Диск
$yd=new FromServerToYa();

echo nl2br('session ID '.session_id().PHP_EOL);
$uploadedFiles = $_SESSION['uploaded-files-object'] ?? new FileToServer();
if (!empty($_FILES))
{
    echo nl2br('Загрузка на хост'.PHP_EOL);
    $uploadedFiles->movingFilesToUpload($_FILES["sentfile"]); //
    $_SESSION['uploaded-files-object']=$uploadedFiles;
}

if (!empty($_POST['send-yd']) && isset($uploadedFiles))
{
    echo nl2br('отправка на ЯД'.PHP_EOL);
    $yd->uploadToYd($uploadedFiles->loadedFilesList);
    $uploadedFiles->clearListFiles();
}


?>
<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="10737418240" />
    <input type="file" multiple name="sentfile[]">
    <input type="submit" name="send-host" value="Загрузить на хост">
</form>

<div <?=isset($uploadedFiles)?'':'hidden'?> >
    <h2>Загружено на хост</h2>
    <ol>
        <?php
        foreach ($uploadedFiles->loadedFilesList as $value){
            echo nl2br('<li>'.$value->name." (размер {$value->size} байт), хэш ".$value->hash."</li>".PHP_EOL);
        }
        ?>
    </ol>
</div>
<form action="" method="post">
    <input type="submit" name="send-yd" value="Отправить на Яндекс Диск">
</form>

<div>
    <h2>Хранится на Яндекс Диске</h2>
    <ol>
        <?php
        foreach ($yd->readListFilesFromYd() as $value){
            echo nl2br('<li><img src="'.$value->preview.'" alt=""><a href='
                .$value->tempLink.'> Файл '.$value->name.' (размер '.$value->size.' байт), хеш '
                .$value->hash
                .'<a href="'.$value->docViewer.'">   ViewDoc</a>'
                .'</a></li>'
                .PHP_EOL);
        }
        ?>
    </ol>
</div>

<?php
require __DIR__.'/../templates/footer.php';
?>

