<?php

namespace App;

class FileToServer
{
    private $uploadsPath;
    public $loadedFilesList=[];
    public function __construct(){
        $this->uploadsPath=$_SERVER['DOCUMENT_ROOT'].'/'.$_ENV['DIR_UPLOAD']; // задан путь хранения файлов не сервере
        echo nl2br('FileToServer  инициализирован'.PHP_EOL);
    }
    public function movingFilesToUpload(array $filesList){
            foreach ($filesList["error"] as $key => $value) { // итерируемся по массиву внутри error
                if ($value == UPLOAD_ERR_OK && is_uploaded_file($filesList['tmp_name'][$key])) { // если error = 0
                    $result=[];
                    foreach ($filesList as $type=>$set){
                        $result[$type]=$set[$key];
                    }
                    echo nl2br('файл в обработке '.$filesList['name'][$key].PHP_EOL);
                    $idContainer=bin2hex(random_bytes(10)); // уникальная hex строка
                    echo nl2br('сгенерирован контейнер '.$idContainer);
                    $result['idContainer']=$idContainer;
                    $this->loadedFilesList[]=new LoadedFile($result); // добавляем новый объект в список
                    $containerFolder=$this->uploadsPath.'/'.$idContainer; // путь до папки контейнера
                    mkdir($containerFolder); // создаём папку контейнера
                    move_uploaded_file($result['tmp_name'], $containerFolder.'/'.$result['name']); // перемещаем файл в папку uploads
                    echo nl2br($result['name'].' загружен');
                }
                else echo nl2br('Ошибка загрузки файла'.$filesList['name'][$key].' код ошибки: '.$value.PHP_EOL);
        }}
    public function clearListFiles(){
        foreach ($this->loadedFilesList as $value){
            $pathFile=$this->uploadsPath.'/'.$value->idContainer.'/'.$value->name;
            echo nl2br('Удаляем '.$pathFile.PHP_EOL);
            if (unlink($pathFile)){ // удаляем файл
                rmdir($this->uploadsPath.'/'.$value->idContainer); // удаляем папку
            };

        };
        $this->loadedFilesList=[]; // очищаем список объекта
    }


}