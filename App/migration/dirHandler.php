<?php

function dirHandler($object, $disk, $KBLink, $idArticle): array
{
    $objectPath=$object->get('path'); // получаем путь до папки
    $objResource=$disk->getResource($objectPath); // получаем ресурс до папки
    $objList=$objResource->items; // список фалов в папке
    $resulObject=['type'=>'dir'];
    if (!count($objList)) $resulObject['content']=["123456789123456789123456789"=>false];
    foreach ($objList as $item){
        $itemName=$item->get('name');
        $itemSize=$item->get('size');
        echo $item->get('type').PHP_EOL;
        if ($item->isDir()){
            $result=dirHandler($item, $disk, $KBLink, $idArticle, $itemSize);
        }
        if ($item->isFile()){
            $result=fileHandler($item, $disk, $KBLink, $idArticle, $itemSize);
        }
        $resulObject['content'][$itemName]=$result;
    }
    return $resulObject;
}