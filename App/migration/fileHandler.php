<?php
function fileHandler($object, $disk, $KBLink, $idArticle, $fileSize): array
{
    echo 'обрабатываем файл: '.$object->get('name').PHP_EOL;
    $hashFile=$object->get('md5');
    $idContainer=uploadFileToYad($object, $disk, $KBLink, $idArticle, $fileSize);
    return [
        'hash'=>$hashFile,
        'type'=>'file',
        'idContainer'=>$idContainer
    ];

}