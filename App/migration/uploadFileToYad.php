<?php
function uploadFileToYad($object,$disk , $KBLink, $idArticle, $fileSize){
    $hashFile=$object->get('md5');
    $prepareSQLReq="SELECT containerName, countLinks FROM knowledgebase_uploaded_files WHERE fileHash='$hashFile'";
    $resultRequest=$KBLink->query($prepareSQLReq, MYSQLI_USE_RESULT);
    if ($KBLink->errno) {
        die('Select Error (' . $KBLink->errno . ') ' . $KBLink->error);
    }
    $fileName=$object->get('name'); // получаем имя файла
    $idContainer=$resultRequest->fetch_all();
    if ($idContainer) {
        $incCount=$idContainer[0][1]+1; // инкрементация счётчика
        $prepareSQLReq="UPDATE knowledgebase_uploaded_files SET countLinks='$incCount' WHERE fileHash='$hashFile'";
        $KBLink->query($prepareSQLReq, MYSQLI_USE_RESULT);
        if ($KBLink->errno) {
            die('Update Error (' . $KBLink->errno . ') ' . $KBLink->error);
        }
        toLog($KBLink, $fileName, $hashFile, $idContainer[0][0], 'file_existed', $object->getPath(), $idArticle, $fileSize);
        return $idContainer[0][0];} // такой файл уже есть
    else {
        $idContainer=random_str(16); // имя контейнера
        $folder = $disk->getResource('app:/' . $idContainer); // создание папки
        try {
            $folder->create(); // создание папки
            toLog($KBLink, $fileName, $hashFile, $idContainer, 'start_moving', $object->getPath(), $idArticle, $fileSize);
            $object->move("app:/$idContainer/$fileName", true);
            $prepareSQLReq="INSERT INTO knowledgebase_uploaded_files (containerName, taskStatus, fileHash, countLinks) VALUES ('$idContainer', 'loaded', '$hashFile', 1)";
            $KBLink->query($prepareSQLReq, MYSQLI_USE_RESULT);
            if ($KBLink->errno) {
                die('Update Error (' . $KBLink->errno . ') ' . $KBLink->error);
            }
            toLog($KBLink, $fileName, $hashFile, $idContainer, 'moved', $object->getPath(), $idArticle, $fileSize);
        }catch (Exception $exception){
            print($exception);
        }
        return $idContainer;
    }


}