<?php

namespace App;

class ErrorHandler extends \Error
{
    public function register(){
        set_error_handler([$this, 'errorHandler']);

    }
    public function errorHandler($errno, $errorstr, $file, $line){
        $this->showError($errno, $errorstr, $file, $line);
        return true;

    }
    protected function showError($errno, $errorstr, $file, $line, $status=500)
    {
        header("HTTP/1.1 {$status}");
        echo "Номер ошибки".$errno
            ."<hr>"."Текст ошибки: ".$errorstr
            ."<hr>"."File: "
            .$file."<hr>"
            ."Line:".$line;
    }
}