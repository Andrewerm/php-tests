<?php

namespace App;

class LoadedFile
{
    public $name;
    public $size;
    public $type;
    public $idContainer;
    public $hash;
    public $tempLink;
    public $preview;
    public $docViewer;


    public function __construct(array $list)
    {
        foreach ($list as $key => $value){
            if ($key!='tmp_name' && $key!='error'){ //tmp_name не нужен
            if ($key=='name') {
                $this->$key = basename($value); // получаем имя файла basename() может предотвратить атаку на файловую систему;
            }
            else {
                $this->$key=$value;
            }}

        }
        $this->hash=hash_file('sha256', $list['tmp_name']); // определяем hash
    }
    public function addYaMetaInfo(array $arrayMeta, string $tempLink){
        $this->preview=$arrayMeta['preview'];
        $this->docViewer=$arrayMeta['docviewer'];
        $this->tempLink=$tempLink;

    }

}