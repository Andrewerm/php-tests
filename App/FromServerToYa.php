<?php

namespace App;
use Arhitector\Yandex\Disk;

class FromServerToYa
{
    private $disk; // экземпляр SDK Яндекс диск
    private $resource; // ссылка на папку приложения
    public function __construct()
    {
        $apikey=$_ENV['YD_API_KEY'];
        $this->disk = new Disk($apikey);
        $freeSpace=round($this->disk['free_space']/1024/1024);
        $this->resource = $this->disk->getResource('app:/');
        echo nl2br('Яндекс диск подключен. Свободное место: '.$freeSpace.' Мб'.PHP_EOL);
    }
    public function readListFilesFromYd (): array
    {
        $items=$this->resource->items->getIterator(); // получаем коллекцию папок в итерируемый массив
//        var_dump($items);
        $result=[];
        foreach ($items as $item)
        {
            $elem=unserialize($item->getProperty('metaInfo')); // получение метаданных папки в виде массива
            $path=$item->getPath(); // получаем путь к папке
            $folderResource=$this->disk->getResource($path); // по пути получаем ссылку на ресурс папки
            $fileInFolder=$folderResource->items->getFirst(); // в папке всегда только один файл, поэтому берем getFirst и получаем ссылку на ресурс файла
            $fileInFolder->setPreview('S'); // размеры превью S, M, L, XL, XXL, XXXL, <ширина>, <ширина>x, x<высота>, <ширина>x<высота>
            $fileInFolder->setPreviewCrop(true); // обрезать ли под размеры заданные в setPreview
            $elem->addYaMetaInfo($fileInFolder->toArray(['preview', 'docviewer']),$fileInFolder->getLink() ); // выбираем поля, необходимые для добавления в объект
            $result[]=$elem; // добавление метаданных в массив
        }
        return $result;
    }

    public function uploadToYd(array $list){
        $uploadsPath=$_SERVER['DOCUMENT_ROOT'].'/'.$_ENV['DIR_UPLOAD'];
//        echo nl2br('загрузка файлов из'.$uploadsPath.PHP_EOL);
        foreach ($list as $value){
            echo nl2br('загружаем на ЯД файл '.$value->name.PHP_EOL);
            echo nl2br('проверка хэш '.PHP_EOL);
            $checkExist=$this->getResourceByHash($value->hash); // проверка на существование такого файла
            if ($checkExist) {
                echo nl2br('такой файл уже загружен'.PHP_EOL);
                var_dump($checkExist);}
                else{
                    echo nl2br('загрузили'.PHP_EOL);
                    $fp = fopen($uploadsPath.'/'.$value->idContainer.'/'.$value->name, 'rb');
                    $folder=$this->disk->getResource('app:/'.$value->idContainer); // создание папки
                    $folder->create(); // создание папки
                    $folder->metaInfo=serialize($value); // прописываем метаописание файла
                    $folder->hash=$value->hash; // прописываем хэш для поиска
                    $resource=$this->disk->getResource('app:/'.$value->idContainer.'/'.$value->name);
                    $resource->upload($fp, 'rb');
                }
        }
    }

    private function getResourceByHash($hash){
        $countFilesInYd=$this->resource->items->count(); // кол-во файлов, загруженных на ЯД
        if (!$countFilesInYd) {return false;}; // если загруженных файлов нет, то false
        $items=$this->resource->items->getIterator(); // получаем коллекцию папок в итерируемый массив
        $iterator=0;
        while ($iterator<$countFilesInYd && $items[$iterator]->getProperty('hash')!=$hash)
        {
            $iterator++;
        }
        return $iterator<$countFilesInYd?unserialize($items[$iterator]->getProperty('metaInfo')):false;

    }


}